package com.freewayso.image.combiner.enums;

/**
 * @Author zhaoqing.chen
 * @Date 2020/8/21
 * @Description 文字Y轴坐标参考基线
 **/

public enum BaseLine {
    /**
     * 文字顶部
     */
    Top,
    /**
     * 文字中间
     */
    Middle,
    /**
     * 文字底部
     */
    Bottom,
    /**
     * 基线
     */
    Base
}
